import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { useRoute } from '@react-navigation/native'
import { PostType } from '../../App'
import { RouteProp } from '@react-navigation/native'
import { useGlobalContext } from '../context'



export const PostItemScreen = () => {
    const [post, setPost] = useState<PostType>()
    const {getPostItemById} = useGlobalContext()

    const route = useRoute<RouteProp<any, 'PostItemScreen'>>()

    useEffect(() => {
        const postItem = getPostItemById(route.params?.id)
        setPost(postItem)
    }, [])
    return (
        <View style={styles.wrapper}>
            <Text style={styles.text}>Id: {post?.id}</Text>
            <Text style={styles.text}>Text: {post?.text}</Text>
            <Text style={styles.text}>Value: {post?.value}</Text>
        </View>
    )
}


const styles = StyleSheet.create({
    wrapper: {
        padding: 10,
        flexDirection: 'column',
        alignItems: 'flex-start'
    },
    text: {
        fontSize: 24
    }
})