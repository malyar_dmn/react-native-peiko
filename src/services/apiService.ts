import { PostType } from '../../App'
import server from '../../server'

type Response = {
    status: number
    data: {
        data: PostType[]
    }
}

const getPosts = (pageNumber: number): Promise<Response> => server.get({page: pageNumber})

export const apiService = {
    getPosts
}