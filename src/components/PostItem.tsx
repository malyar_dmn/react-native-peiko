import React from 'react'
import { StyleSheet, Text, View, Pressable } from 'react-native'
import { PostType } from '../../App'

type Props = {
    postItem: PostType
    onPress: (item: PostType) => void
}

export const PostItem = (props: Props) => {
    const {postItem, onPress} = props

    return (
        <Pressable onPress={() => onPress(postItem)}>
            <View style={styles.postItem}>
                <Text style={styles.postItemTitle}>{postItem.id}</Text>
                <Text style={styles.postItemTitle}>{postItem.text}</Text>
                <Text style={[styles.postItemValue, postItem.value > 0 ? {color: 'blue'} : {color: 'red'}]}>{postItem.value}</Text>
            </View>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    postItem: {
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: 'black',
        paddingVertical: 15,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    postItemTitle: {
        fontSize: 18
    },

    postItemValue: {

    }
})