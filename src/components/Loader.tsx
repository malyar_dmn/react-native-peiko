import React from 'react'
import { ActivityIndicator, StyleSheet, View } from 'react-native'

export const Loader = () => {
    return (
        <View style={styles.loaderWrapper}>
            <ActivityIndicator size="large" color="blue" />
        </View>
    )
}

const styles = StyleSheet.create({
    loaderWrapper: {
        width: '100%',
        height: '40%',
        position: "absolute",
        bottom: 0,
        left: 0,
        justifyContent: 'center',
        alignItems: 'center'
    }
})