import React, {useEffect, useState} from "react";
import { StyleSheet, Text, View, RefreshControl, Alert } from "react-native";
import { PostType } from "../../App";
import { useGlobalContext } from "../context";
import { PostItem } from "./PostItem";
import { apiService } from "../services/apiService";
import { FlatList } from "react-native-gesture-handler";
import { Loader } from "./Loader";


type Props = {
  posts: PostType[];
  navigation: any
};

export const PostList = (props: Props) => {
  const {state, setPosts} = useGlobalContext()

  const [page, setPage] = useState<number>(0)
  const [refreshing, setRefreshing] = useState<boolean>(false)
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [error, setError] = useState<string>('')

  const {navigation} = props

  const listItemPressHandler = (postItem: PostType) => {
    navigation.navigate('PostItemScreen', {id: postItem.id})
  }

  useEffect(() => {
    loadPostsPage(page)
  }, [page])

  useEffect(() => {
  }, [state.posts.length])


  const loadPostsPage = (page: number) => {
    setIsLoading(true)
    apiService.getPosts(page)
      .then(res => {
        setPosts(res.data.data)
        setIsLoading(false)
        setRefreshing(false)
        
      })
      .catch(err => {
        if (err.status !== 200) {
          setError(err.data.message)
          showAlert(err.data.message)
        }
      })
  }

  const onEndReachedHandler = () => {
    if (error) {
      return
    }
    setPage(prev => prev + 1)
  }

  const handleRefresh = () => {
    setRefreshing(true)
    setPage(0)
    loadPostsPage(page)
  }

  const showAlert = (errorMsg: string) => {
    Alert.alert('Error occured', errorMsg, [
      {text: 'Ok', onPress: () => {
        setError('')
        if (page === 0) {
          loadPostsPage(page)
        }
        setPage(prev => prev = prev)
        loadPostsPage(page)
        console.log('page on error', page)
        return
      }}
    ])
  }


  return (
    <View>
    {isLoading && <Loader />}

      <FlatList contentContainerStyle={{paddingBottom: 200}} 
        data={state.posts}
        renderItem={({item}) => <PostItem postItem={item} onPress={listItemPressHandler}/>}
        keyExtractor={(item, index) => index.toString()}
        onEndReached={onEndReachedHandler}
        onEndReachedThreshold={0.1}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => handleRefresh()}
            enabled={true}
          />
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  postList: {
    height: "100%",
  },
  
});
