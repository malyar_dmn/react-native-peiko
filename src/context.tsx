import React, { createContext, useContext, useEffect, useState } from 'react'
import { PostType } from '../App'

const GlobalContext = createContext<GlobalController>({} as GlobalController)

interface Props {
    children: any;
}

const GlobalContextProvider = (props: Props) => {
    const [data, setData] = useState<GlobalState>({
        posts: []
    })

    const setPosts = (posts: PostType[]) => {
        setData((prevState) => {
            const existedPosts = prevState.posts
            return {
                ...prevState,
                posts: [...existedPosts, ...posts]
            }
        })
    }

    const getAllPosts = (): PostType[] => {
        return data.posts
    }

    const getPostItemById = (id: number): PostType | undefined => {
        return data.posts.find((post: PostType) => post.id === id)
    }

    const globalState: GlobalController = {
        state: data,
        getAllPosts,
        getPostItemById,
        setPosts
    }

    return (
        <GlobalContext.Provider value={globalState}>
            {props.children}
        </GlobalContext.Provider>
    )
}

export interface GlobalController {
    state: GlobalState,
    getAllPosts: () => PostType[]
    getPostItemById: (id: number) => PostType | undefined
    setPosts: (posts: PostType[]) => void
}


export interface GlobalState {
    posts: PostType[]
}
  

export default GlobalContextProvider

export const useGlobalContext = () => useContext<GlobalController>(GlobalContext)