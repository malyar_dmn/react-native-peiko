import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { PostList } from './src/components/PostList';
import GlobalContextProvider from './src/context';
import { PostItemScreen } from './src/screens/PostItemScreen';

export type PostType = {
  id: number | string
  text: string
  value: number | string
}

const PostsStack = createStackNavigator()

export default function App() {


  return (
    <GlobalContextProvider>
      <NavigationContainer>
        <PostsStack.Navigator>
          <PostsStack.Screen name="PostList" component={PostList} />
          <PostsStack.Screen name="PostItemScreen" component={PostItemScreen} />
        </PostsStack.Navigator>
      </NavigationContainer>
    </GlobalContextProvider>
  );
}
